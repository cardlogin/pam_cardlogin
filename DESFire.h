/*
 *  DESFire.h is part of PAM CARDlogin Module 
 *  Copyright (C) 2010 Frank Engler <pam_CARD@web.de>,
 *
 *  This file is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This file is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this file.  If not, see <http://www.gnu.org/licenses/>.
 *
 * This library is based on pam_p11 by Mario Strasser <mast@gmx.net>
 * and on the sample program to use PC/SC API by Ludovic Rousseau
 * <ludovic.rousseau@free.fr>.
 */

#define SMARTCARD_NAME "DESFire/OWOK light"

BYTE SmartcardAtr[] = { 0x3B, 0x81, 0x80, 0x01, 0x80, 0x80 };
BYTE SmartcardCommandGetVersion[] = { 0x90, 0x60, 0x00, 0x00, 0x00 };
BYTE SmartcardCommandFetchMoreData[] = { 0x90, 0xAF, 0x00, 0x00, 0x00 };
